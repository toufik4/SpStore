# Microsoft Developer Studio Project File - Name="SpStore" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=SpStore - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SpStore.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SpStore.mak" CFG="SpStore - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SpStore - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "SpStore - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "SpStore"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SpStore - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Z7 /O2 /I "$(GCL_XT_CPP)" /I "$(GCL_SPOCC)" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /n
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /pdb:none /machine:I386
# Begin Special Build Tool
TargetPath=.\Release\SpStore.exe
TargetName=SpStore
SOURCE="$(InputPath)"
PostBuild_Cmds=IF NOT EXIST "$(GCL_SPOCC)\SPOCC\lib" MKDIR "$(GCL_SPOCC)\SPOCC\lib"	COPY "$(TargetPath)" "$(GCL_SPOCC)\SPOCC\lib\$(TargetName).lib"
# End Special Build Tool

!ELSEIF  "$(CFG)" == "SpStore - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MT /W3 /Gm /GR /GX /Zi /Od /I "$(GCL_XT_CPP)" /I "$(GCL_SPOCC)" /I "$(ILSHOME)\include" /I "$(CRASHHANDLER)" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "ILSSTD" /D "DBNTWIN32" /D "XTWIN_USELOGGER" /FR /YX /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /n
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib ntwdblib.lib wsock32.lib user32.lib imm32.lib mvcomp.lib mvtcp.lib ilxml.lib mvserver.lib server.lib crashhandler.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"../SpCmn/Lib" /libpath:"../SpRecon/Lib" /libpath:"$(ILSHOME)\lib\msvc6\$(FORMAT)" /libpath:"$(ILVHOME)\lib\msvc6\$(FORMAT)" /libpath:"$(REGEX_PATH)\lib" /libpath:"$(DBGHLP_PATH)" /libpath:"$(CRASHHANDLER)\bin"
# SUBTRACT LINK32 /pdb:none
# Begin Special Build Tool
TargetPath=.\Debug\SpStore.exe
TargetName=SpStore
SOURCE="$(InputPath)"
PostBuild_Cmds=IF NOT EXIST "$(GCL_SPOCC)\SPOCC\lib" MKDIR "$(GCL_SPOCC)\SPOCC\lib"	COPY "$(TargetPath)" "$(GCL_SPOCC)\SPOCC\lib\$(TargetName).lib"
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "SpStore - Win32 Release"
# Name "SpStore - Win32 Debug"
# Begin Group "sources"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Code\SpStoreMain.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=".\Sample data\sample_store.csv"
# End Source File
# Begin Source File

SOURCE=".\Sample data\sample_store.ILS"
# End Source File
# End Target
# End Project
