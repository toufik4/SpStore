#pragma once

//*****************************************************************************
//
//  Copyright (c) Matra Transport International 2001
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version
//  associ� (.ver).
//
//  Fichier  : SpDB_DBLib.h
//  Auteur   : Jean BRETAUDEAU
//  Cr�ation : jeudi 13 septembre 2001
//  Contenu  : D�claration de la classe SpDB_DBLib
//
//*****************************************************************************
//  Modifications : 21335_OT_1 : A. Kolybine : Gestion des deconnexions SQL Server.
//								 Rajout de la m�thode virtuelle onDisconnection()
//*****************************************************************************
/*
#ifndef DB_ENGINE_DBLIB
#define DB_ENGINE_DBLIB
#endif
*/
#ifdef DB_ENGINE_DBLIB

#ifndef DBNTWIN32
#define DBNTWIN32
#endif

#include <XtWin/Interface/XtWinWindowsApi.h>
#include <sqlfront.h>
#include <sqldb.h>
#include <ilserver/sstring.h>
#include <SpStore/Interface/SpStoreDBMgr.h>
/*
#define STORE_MANAGER_MAX_ID_LENGTH 256
#define STORE_MANAGER_MAX_ATTR_LENGTH 256
#define STORE_MANAGER_MAX_VALUE_LENGTH 256
*/

//.=================================================================
//.Class : SpDB_DBLib 
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0	28/01/99 Creation : Marie-H�l�ne Orluc
//.			13/02/01 Modif : MHO . La classe est transform�e en classe 
//.					 singleton. Les anciennes m�thodes statiques sont gard�es
//;					 pour des raisons de compatibilit� 
//. 
//.Inheritance Path : none
//.---------------- 
//. 
//.Description : ensemble de m�thodes permettant le cr�ation des instances
//.-----------	 du mod�le de donn�es SPOCC � partir de table SQL.
//. 
//.Limitation : %%%TBD
//.---------- 
//. 
//.=================================================================
class SpDB_DBLib
{
public :
	//##Documentation
	//## Destructeur.
	virtual ~SpDB_DBLib();

	//##Documentation
	//## Ouverture de la connection SQL serveur.
	virtual void initConnect(const char* loginName, const char* loginPasswd,
		const char* processName, const char* serverName, const char* baseName, const int timeOut = 1);
	
	//##Documentation
	//## fermeture de la connection SQL serveur. 
	virtual void closeConnect();

	//##Documentation
	//## Ex�cution de la requ�te SQL 
	virtual DBPROCESS* executeQuery(const char* command); 

	virtual DBPROCESS* getDbproc() {
		return _dbproc; }

    bool isGoodConnection() const {
        return _dbproc != 0; }
    
	//##Documentation
    //## D�but de transaction  : m�thode � appeler avant tout groupe  
	//## de requ�tes d'�criture ou de mise � jour � �x�cuter d'un seul bloc.
	void begin_transaction();

	//##Documentation
	//## Fin de transaction : � utiliser � la fin de chaque fin de groupes 
	//## de transactions. 
	void commit_transaction();

	//##Documentation
	//## Indique si une transaction est en cours.
	bool isTransactionEnCours()	const {
		return _transaction_en_cours; }

	//##Documentation
	//## Positionne l'indicateur de transaction en cours.
	void setTransactionEnCours(bool transaction_en_cours) {
		_transaction_en_cours = transaction_en_cours; }

	SpDB_DBLib();

	// Wrapping DBLib API Routines :
	void BeginTransaction();
	void CommitTransaction();
	void AddQuery(const IlsString& req);
	void AddQuery(const std::string& req);
	void ExecQuery();
	void CancelQuery();
	bool HasLastQuerySucced();
	bool NoMoreResults();
	bool Failed();
	// virtual DBPROCESS* getDbproc();

	//void bind(SpStoreDBMgr::t_Row& row);
	/*
	class t_Row {
		public:
			//##Documentation
			//## bind request results to this class attributes.
			virtual void bind() {};
	};

	//##Documentation
	//## Permet de r�cup�rer les r�sultats d'une requ�te SQL qui porte sur des identifiants seulement.
	class t_IdRow : public t_Row {
		public:
			//##Documentation
			//## Constructeur.
			t_IdRow(SpDB_DBLib* data_base);

			//##Documentation
			//## bind request results to this class attributes.
			void bind();

			//##Documentation
			//## Identifiant d'objet du serveur dans la base Store.
			const char* getId() const { return _id; }

		protected:

			//##Documentation
			//## bind request columns to this class attributes.
			virtual void bindRegularResultColumns();

			//##Documentation
			//## lien vers le manager de BD.
			SpDB_DBLib* _data_base;

		private:
			//##Documentation
			//## Identifiant d'objet du serveur dans la base Store.
			char _id[STORE_MANAGER_MAX_ID_LENGTH];

			//##Documentation
			//## Indique si le lien a �t� �tabli entre les attributs de la classe et les r�sultats d'une requ�te sur la base.
			bool _bound;
	};

	//##Documentation
	//## Permet de r�cup�rer les r�sultats d'une requ�te SQL qui porte sur les identifiants, les noms des attributs et leurs valeurs.
	class t_RestoreRow : public t_IdRow { 
		public:
			//##Documentation
			//## Constructeur.
			t_RestoreRow(SpDB_DBLib* data_base);

			//##Documentation
			//## Nom d'attribut d'un objet RP dans la base Store.
			const char* getAttr() const {
				return _attr; 
			}

			//##Documentation
			//## Valeur associ�e � un attribut d'un objet RP dans la base Store.
			const char* getValue() const {
				return _value; 
			}

		protected:

			//##Documentation
			//## bind request columns to this class attributes.
			virtual void bindRegularResultColumns();

		private:
			//##Documentation
			//## Nom d'attribut d'un objet RP dans la base Store.
			char _attr[STORE_MANAGER_MAX_ATTR_LENGTH];

			//##Documentation
			//## Valeur associ�e � un attribut d'un objet RP dans la base Store.
			char _value[STORE_MANAGER_MAX_VALUE_LENGTH];
	};

	// Rows Manipulation
	bool NextRow(t_Row& row);

	friend t_Row;

	friend t_RestoreRow;
*/
protected: 

	//##Documentation
	//## Fonction appel�e en cas de perte du serveur SQL.
	virtual void onDisconnection();
	
	//##Documentation
	//## Gestion des erreurs SQL  : appelant la m�thode errorSQL pouvant �tre d�riv�e 
	static	 int err_handler(DBPROCESS*, int, int, int, char*, char*);
	//##Documentation
	//## Gestion des erreurs SQL.
	virtual void errorSQL(DBPROCESS*, int, int, int, char*, char*);

	// pointeur sur la structure utilis�e pour l'utilisation des 
	// DB library 
	DBPROCESS*	_dbproc; 

    // pour fermer l'ancienne connexion avant l'ouverture d'une nouvelle connexion 
    // Remarque : la fermeture de l'ancienne connexion est retard�e pour �viter qu'une
    // utilisation soit faite d'un dbproc avec une connexion ferm�e ...
   	DBPROCESS*	_old_dbproc;

	bool _transaction_en_cours;

	static bool _is_initialised;

private : 

};

#endif DB_ENGINE_DBLIB