#pragma once

//.=================================================================
//.
//.        Copyright (c) Matra Transport International 2000
//.        Ce fichier est juridiquement prot�g� par la loi 98-531  
//.        du 1er juillet 1998 
//.        La version et les �volutions sont identifi�es dans le  
//.        fichier de version associ� .ver
//.
//.        File : SpStoreRp.h
//.        ----
//.
//.        Content : >> describe the purpose of the file <<
//.        -------
//.
//.=================================================================

#include <SpRecon/Interface/SpReconRpObject.h>
#include <SpCmn/Interface/SpCmnDate.h>

#include <xtl/threading/synchronized_value.h>

#include <map>
#include <list>
#include <string>
#include <queue>

static class SpStoreDummy_ToForceLink {public: SpStoreDummy_ToForceLink();} SpStoreDummy_ToForceLink_;

//.=================================================================
//.Class : SpStoreServerStateRp
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 02/11/00 Creation : Alexis Kolybine
//. 
//.Inheritance Path : 
//.---------------- 
//. SpReconRpObject 
//. -> SpStoreRp
//. 
//.Description : >> describe the purpose of the class <<
//.-----------   
//. 
//.Limitation : none
//.---------- 
//. 
//.=================================================================
class SpStoreRp : public IlsRpObject//SpReconRpObject
{
public :
	SpStoreRp (IlsRepresentation& rp, const IlsRpObjModel& m)
	   : IlsRpObject(rp, m) {;}
//	   : SpReconRpObject(rp, m) {;}


	const IlsString& getId() const { return _id; }

	virtual void store(const IlsString& attr, const IlsMvValue& value);

	//Le cas d'un �l�ment absent de la map n'est pas trait�
	//car consid�r� comme impossible.
	IlsString getAttributeValue(IlsString attr) {
		return _rpValues[attr]; }

	// Renvoie une cha�ne de caract�res dont toutes les simples quotes ont �t� remplac�es par des doubles quotes.
	static std::string getSQLCompliantString(const IlsMvValue& value);

protected : 

	virtual void setId(const IlsString &id);  

	std::map<IlsString, IlsString> _rpValues;

	IlsString _id;

private : 
	ILS_RP_OBJECT_DECL(SpStoreRp)
};


class SpStoreEmptyRp : public IlsRpObject
{
public :
	SpStoreEmptyRp (IlsRepresentation& rp, const IlsRpObjModel& m)
	   : IlsRpObject(rp, m) {;}

ILS_RP_OBJECT_DECL(SpStoreEmptyRp)
};


//.=================================================================
//.Class : SpStoreDynRp
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 11/12/03 Creation : ENA
//. 
//.Inheritance Path : 
//.---------------- 
//. + SpStoreRp
//.   + IlsRpObject
//. 
//.Description : >> describe the purpose of the class <<
//.-----------   
//. 
//.Limitation : Les objets dynamiques doivent avoir un identifiant
//.----------   unique pour etre r�f�renc� correctement dans la
//.             table de 'store' (corr�lation store/restore).
//.
//.             Cette classe g�n�re automatiquement des attributs
//.             sp�cifiques dans la base de donn�e :
//.
//.               - IS_DYNAMIC : indique pour le restore que l'objet
//.                              est � cr�er dynamiquement.
//.
//.               - HAS_OWNER : indique le nom du conteneur de l'objet
//.                             quand il est poss�d�.
//.
//.               - SV_TYPE : indique le nom de la classe cot� serveur
//.                           pour la construction dynamique de l'objet.
//.
//.               - COLLECTOR : indique le nom du collecteur de l'objet
//.                             dans le conteneur.
//. 
//.=================================================================
class SpStoreDynRp : public SpStoreRp
{
public :
	SpStoreDynRp (IlsRepresentation& rp, const IlsRpObjModel& m)
	   : SpStoreRp(rp, m) {;}

	virtual ~SpStoreDynRp();

	virtual void store(const IlsString& attr, const IlsMvValue& value);

protected :
	//##Documentation
	//## Methode appell�e sur creation du Rp (associ�e � une macro ILOG)
	void setOwner(SpStoreRp* owner);

	//##Documentation
	//## M�thode supprimant les enregistrements en base de donn�e associ�s
	//## � un objet ainsi que les enregistrements des objets qu'il peut
	//## poss�der.
	void deleteRowAndChild(const IlsString& rowId, const IlsString& table);

	//## Attribute: _owner
	//   Poiteur sur le responsable de la creation de l'instance.
	SpStoreRp* _owner;

private : 
	ILS_RP_OBJECT_DECL(SpStoreDynRp)
};


//.=================================================================
//.Class : SpStoreRepresentation
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 03/11/00 Creation : Alexis Kolybine
//. 
//.Inheritance Path : 
//.---------------- 
//. SpReconRepresentation 
//. -> SpStoreRepresentation
//. 
//.Description : >> describe the purpose of the class <<
//.----------- 
//. 
//.Limitation : >> for example singleton class, dependency ... <<
//.---------- 
//. 
//.=================================================================


class SpStoreRepresentation : public IlsRepresentation//SpReconRepresentation
{
 public:
  SpStoreRepresentation(IlsMvComponent& c, const IlsRpModel& m);

  ~SpStoreRepresentation();

  void setHost(const IlsString& h);
  void setSchema(const IlsString& s)	{ _schema = s; }
  void setTable(const IlsString& t)		{ _table = t; }
  const IlsString& getTable()			{ return _table; }

  IlsBoolean beginS2CTransaction(IlsS2CTransStatus transStatus,
                               IlsTransactionId);

  void endS2CTransaction(IlsS2CTransStatus transStatus,
                               IlsTransactionId);


  static void DoStore(const unsigned long timeout);

  void addObject(SpStoreRp* o);


  typedef std::pair<IlsString,IlsString> StoreId;
  void eraseObject(StoreId id) {
	  _stored_objects.erase( id );
  }


  bool isCreated() const {
	  return _is_created;
  }

  IlsBoolean isInDynList(const IlsString& id, const IlsString& attr);

  void addDynList(const IlsString& id, const IlsString& attr);

  // commun � l'ensemble des repr�sentations - anciennes et nouvelles repr�sentations
  // --------------------------------------------------------------------------------
  static void OnDBReconnection();

  // flag base de donn�e d�connect�e et non encore reconnect�e compl�tement
  static bool IsDBDisconnection() {
      return _isDBDisconnection; }

  // reconnexion avec le serveur 
  static bool IsReconnecting() {
      return _isReconnecting; }

protected:

  void initialiseTable();
  IlsString _host; 
  IlsString _schema; 
  IlsString _table; 


  std::map<StoreId, SpStoreRp*> _stored_objects;
  std::list<std::string> _dyn_id_attribut_list;
  bool _is_created;

 private:
    // representation utilis�e pour la d�connexion serveur
    static SpStoreRepresentation* _representationActive;

    // flag base de donn�e d�connect�e et non encore reconnect�e compl�tement
	// Utilisation d'un mod�le de donn�es thread safe car la variable est mise � jour dans le thread principal et le thread de vidage de la file de requ�te
    static xtl::threading::synchronized_value<bool> _isDBDisconnection;

    // reconnexion avec le serveur
    static bool _isReconnecting;

  ILS_REPRESENTATION_DECL(SpStoreRepresentation)
};





