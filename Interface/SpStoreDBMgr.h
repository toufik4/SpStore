#pragma once

//.=================================================================
//.
//.        Copyright (c) Matra Transport International 2000
//.        Ce fichier est juridiquement prot�g� par la loi 98-531  
//.        du 1er juillet 1998 
//.        La version et les �volutions sont identifi�es dans le  
//.        fichier de version associ� .ver
//.
//.        File : SpStoreDBMgr.h
//.        ----
//.
//.        Content : >> describe the purpose of the file <<
//.        -------
//.
//.=================================================================

//#define DB_ENGINE_DBLIB
/*
#define STORE_MANAGER_MAX_ID_LENGTH 256
#define STORE_MANAGER_MAX_ATTR_LENGTH 256
#define STORE_MANAGER_MAX_VALUE_LENGTH 256
*/
class SpDBEngine;

//#include <SpCmn/Interface/SpCmnSQL.h>
//#include <SpStore/Interface/SpDB_DBLib.h> // TAB

#include <SpCmn/Interface/SpCmnDate.h>

#include <SpStore/Interface/SpLogStore.h>

#include <xtl/threading/synchronized_queue.h>

#include <ilserver/sstring.h>



//.=================================================================
//.Class : SpStoreDBMgr
//.----- 
//. 
//.Historical : 
//.---------- 
//.  1.0 03/11/00 Creation : Alexis Kolybine
//. 
//.Inheritance Path : 
//.---------------- 
//. SpCmnQuerySQL 
//. -> SpStoreDBMgr
//. 
//.Description : >> describe the purpose of the class <<
//.----------- 
//. 
//.Limitation : >> for example singleton class, dependency ... <<
//.---------- 
//. 
//.=================================================================
class SpStoreDBMgr /* : public SpCmnSQL */ // TAB
{

public :
	/*
	// BEG Row Definitions
	class t_Row {
	public:
		//##Documentation
		//## bind request results to this class attributes.
		virtual void bind() {};
	};

	//##Documentation
	//## Permet de r�cup�rer les r�sultats d'une requ�te SQL qui porte sur des identifiants seulement.
	class t_IdRow : public t_Row {
	public:
		//##Documentation
		//## Constructeur.
		t_IdRow(SpStoreDBMgr* data_base);

		//##Documentation
		//## bind request results to this class attributes.
		void bind();

		//##Documentation
		//## Identifiant d'objet du serveur dans la base Store.
		const char* getId() const { return _id; }

	protected:

		//##Documentation
		//## bind request columns to this class attributes.
		virtual void bindRegularResultColumns();

		//##Documentation
		//## Identifiant d'objet du serveur dans la base Store.
		char _id[STORE_MANAGER_MAX_ID_LENGTH];


		//##Documentation
		//## lien vers le manager de BD.
		SpStoreDBMgr* _data_base;

	private:
		//##Documentation
		//## Indique si le lien a �t� �tabli entre les attributs de la classe et les r�sultats d'une requ�te sur la base.
		bool _bound;
	};

	//##Documentation
	//## Permet de r�cup�rer les r�sultats d'une requ�te SQL qui porte sur les identifiants, les noms des attributs et leurs valeurs.
	class t_RestoreRow : public t_IdRow { 
	public:
		//##Documentation
		//## Constructeur.
		t_RestoreRow(SpStoreDBMgr* data_base);

		//##Documentation
		//## Nom d'attribut d'un objet RP dans la base Store.
		const char* getAttr() const {
			return _attr; 
		}

		//##Documentation
		//## Valeur associ�e � un attribut d'un objet RP dans la base Store.
		const char* getValue() const {
			return _value; 
		}

	protected:
		//## bind request columns to this class attributes.

		//##Documentation
		virtual void bindRegularResultColumns();

	private:
		//##Documentation
		//## Nom d'attribut d'un objet RP dans la base Store.
		char _attr[STORE_MANAGER_MAX_ATTR_LENGTH];

		//##Documentation
		//## Valeur associ�e � un attribut d'un objet RP dans la base Store.
		char _value[STORE_MANAGER_MAX_VALUE_LENGTH];
	};

	friend t_Row;

	friend t_RestoreRow;
	*/
	// END Row Definitions
	typedef xtl::threading::synchronized_queue<std::string> synchronized_queue_type;
	typedef std::deque<std::string> queue_type;

	SpStoreDBMgr();
	virtual ~SpStoreDBMgr();

	// Wrapping Methods to DB Engine
	/*
	void BeginTransaction();
	void CommitTransaction();
	void AddQuery(const IlsString& req);
	void AddQuery(const std::string& req);
	void ExecQuery();
	void CancelQuery();
	bool HasLastQuerySucced();
	bool NoMoreResults();
	bool Failed();
	*/
	bool CheckConnection();
	

    virtual void connect();   
	virtual void initConnection(const std::string& loginName, const std::string& loginPasswd,
			const std::string& processName, const std::string& serverName, const std::string& baseName,
			bool reconnectable = false);

	void closeConnection(); 
	
	// bool isGoodConnection() const;
	
    // la base de donn�e a-t-elle �t� initialis�e
    bool isInitialized() {
        // remarque, ne retourne plus si la connexion est bonne - car la gestion de la reconnexion, si 
        // la connexion est invalide, est trait� par getDbproc
        return _is_initialized;
	}

	// Rows Manipulation
	// bool NextRow(t_Row& row);

	//##Documentation
	//## Adds the request req to the synchronized queue of requests to be executed
	void asyncLog(std::string&& req);

	//##Documentation
	//## Uses _queue's waiting_swap to swap _queue with queue (passed as reference argument). argument queue should be empty when calling this function
	bool waiting_swap_pending_queue(queue_type& queue, unsigned long timeout = INFINITE);

	static SpStoreDBMgr* GetInstance();

	SpDBEngine* getDBEngine() { return _ptDBEngine; }

protected : 

	// void errorSQL(DBPROCESS*, int, int, int, char*, char*);

	bool _is_initialized;

    static SpStoreDBMgr* _instance;

private : 
	// DB Engine
	SpDBEngine* _ptDBEngine;  // TAB
    bool _reconnectable;

    std::string _loginName;
	std::string _password;
	std::string _processName;
	std::string _server;
	std::string _baseName;

	// temps entre deux tentatives de connexion au serveur SQL
	static const SpCmnIntervalTime _frozen_window;

    // date du dernier essai de reconnexion
    SpCmnDate _last_connection_try;

	//##Documentation
	//## Synchronized queue of requests to be executed. This object handles concurrent accesses to the queue.
	synchronized_queue_type _queue;

	//##Documentation
	//## Thread object that empties _queue and executes the requests stored in it
	SpLogStore* _queueHandler;
};

