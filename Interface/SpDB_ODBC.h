#pragma once

#ifdef DB_ENGINE_ODBC

#include <SpStore/Interface/SpStoreDBMgr.h>
#include <SpStore/Interface/SpDBEngine.h>
#include <SpStore/Interface/SpODBC.h>
#include <list>

class SpDB_ODBC : public SpDBEngine 
{
public :
	//##Documentation
	//## Destructeur.
	virtual ~SpDB_ODBC();
			 SpDB_ODBC();

	//##Documentation
	//## Ouverture de la connection SQL serveur.
	virtual void initConnect(const char* loginName, const char* loginPasswd,
		const char* processName, const char* serverName, const char* baseName, const int timeOut = 1);
	
	//##Documentation
	//## fermeture de la connection SQL serveur. 
	virtual void closeConnect();

    bool isGoodConnection() const ;
    

	// Wrapping DBLib API Routines :

	void AddQuery(const std::string& req);
	void ExecQuery();
	void CancelQuery();
	bool HasLastQuerySucced();
	bool NoMoreResults();
	bool Failed();



	// Rows Manipulation
	bool NextRow(t_Row& row);
	//void bind1(const char* id);
	//void bind2(const char* id, const char* att, const char* value);
	void bind(t_Row& row);

protected: 

	//##Documentation
	//## Fonction appel�e en cas de perte du serveur SQL.
	virtual void onDisconnection();
	

private : 

	SpODBC::Connection _Connection;
    SpODBC::Command	_Command;
	std::list<std::string> _QList;
};
#endif //DB_ENGINE_DBLIB