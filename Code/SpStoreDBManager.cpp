//*****************************************************************************
//
//  Copyright (c) Siemens Transportation Systems 2003
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version
//  associ� (.ver).
//
//  Fichier  : SpStoreDBManager.cpp
//  Auteur   : Alexis KOLYBINE
//  Cr�ation : mardi 8 avril 2003
//  Contenu  : D�finition de la classe SpStoreDBManager
//
//*****************************************************************************
//  Modifications : 
//*****************************************************************************

#include <SpStore/Interface/SpStoreDBManager.h>
#include <SpStore/Interface/SpStoreTraces.h>
#include <SpStore/Interface/SpStoreRp.h>

const SpCmnIntervalTime SpStoreDBManager::_frozen_window(SpCmnIntervalTime::Milliseconds(120000));

SpStoreDBManager* SpStoreDBManager::_instance = 0;


//.=================================================================
SpStoreDBManager::t_IdRow::t_IdRow(SpStoreDBManager* data_base) :
_data_base(data_base),
_bound(false)
{
}

//.=================================================================
SpStoreDBManager::t_RestoreRow::t_RestoreRow(SpStoreDBManager* data_base) :
t_IdRow(data_base)
{
}

bool SpStoreDBManager::NextRow(SpStoreDBManager::t_Row& row)
{ 
    DBPROCESS* dbproc = SpCmnSQL::getDbproc();

    if (dbproc) {
		row.bind();
		return dbnextrow(dbproc) != NO_MORE_ROWS;
	} else	{
		return false;
	}
}

//.=================================================================
void SpStoreDBManager::t_IdRow::bind()
{
    DBPROCESS* dbproc = _data_base->SpCmnSQL::getDbproc();

    if (!_bound && dbproc) {
		bindRegularResultColumns(dbproc);
		_bound = true;
	}
}

//.=================================================================
void SpStoreDBManager::t_IdRow::bindRegularResultColumns(DBPROCESS* dbproc)
{
	dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)_id); 
}

//.=================================================================
void SpStoreDBManager::t_RestoreRow::bindRegularResultColumns(DBPROCESS* dbproc)
{
	t_IdRow::bindRegularResultColumns(dbproc);

	dbbind (dbproc,2,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ATTR_LENGTH,(unsigned char *)_attr);
	dbbind (dbproc,3,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_VALUE_LENGTH,(unsigned char *)_value);
}

SpStoreDBManager::SpStoreDBManager()
	: SpCmnSQL()
	, _is_initialized(false)
    , _loginName()
    , _password()
    , _processName()
    , _server()
    , _baseName()
    , _reconnectable(false)
   	, _last_connection_try(SpCmnDate::bad)
	, _queueHandler(new SpLogStore())
{
}

SpStoreDBManager::~SpStoreDBManager()
{
	delete _queueHandler;
}

SpStoreDBManager* SpStoreDBManager::GetInstance()
{
    if (! _instance)
        _instance = new SpStoreDBManager();

    return _instance;
}

void	SpStoreDBManager::connect()
{
	initConnect(_loginName.c_str(), _password.c_str(), _processName.c_str(), _server.c_str(), _baseName.c_str());
}

void SpStoreDBManager::initConnection(const std::string& loginName, const std::string& loginPasswd,
	const std::string& processName, const std::string& serverName, const std::string& baseName, bool reconnectable)
{ 
	_loginName  = loginName;
   _password    = loginPasswd;
   _processName = processName;
   _server      = serverName;
   _baseName    = baseName;
   _reconnectable = reconnectable;

	if (!_is_initialized)
    {
	    connect();
		_is_initialized = true; 
	}
}

void SpStoreDBManager::BeginTransaction(DBPROCESS* dbproc)
{ 
	if (dbproc) 
		dbcmd(dbproc, "BEGIN TRANSACTION\n"); 
}

void SpStoreDBManager::CommitTransaction(DBPROCESS* dbproc)
{ 
	if (dbproc) 
		dbcmd(dbproc, "COMMIT TRANSACTION\n");
}

void SpStoreDBManager::AddQuery(DBPROCESS* dbproc, const IlsString& req)
{ 
	if (dbproc && isGoodConnection()) 
		dbfcmd(dbproc, "%s\n", req.value());	
}

void SpStoreDBManager::AddQuery(DBPROCESS* dbproc, const std::string& req)
{ 
	if (dbproc && isGoodConnection()) 
		dbfcmd(dbproc, "%s\n", req.c_str());	
}

void SpStoreDBManager::ExecQuery(DBPROCESS* dbproc)
{ 
	if (dbproc && isGoodConnection()) 
		dbsqlexec(dbproc); 
}

void SpStoreDBManager::CancelQuery(DBPROCESS* dbproc)
{ 
	if (dbproc && isGoodConnection()) 
		dbcancel(dbproc); 
}

bool SpStoreDBManager::HasLastQuerySucced(DBPROCESS* dbproc)
{ 
	return isGoodConnection() && dbresults(dbproc) == SUCCEED; 
}

void SpStoreDBManager::closeConnection()
{ 
	SpCmnSQL::closeConnect(); 
	_is_initialized = false; 
}
 
DBPROCESS* SpStoreDBManager::getDbproc()
{
    // si reconnectable sur panne (store doit �tre reconnectable mais restore non : on ne peut bloquer l'init
    // du serveur si la machine d'archivage est en panne
    if (_reconnectable)
    {
        // si la connexion est morte 
        if (!_dbproc)
        {
            // si le dernier essai de reconnexion date de plus de n secondes
			if (_last_connection_try.isBad() || (SpCmnDate::Now() - _last_connection_try  > _frozen_window))
            {
                // essai de reconnexion
                connect();
                _last_connection_try = SpCmnDate::Now();

                // si reconnexion OK
                if (_dbproc)
                {
                    // prise en compte par la repr�sentation
                    SpStoreRepresentation::OnDBReconnection();
                }
            }
        }
    }
	return _dbproc;
}

void SpStoreDBManager::asyncLog(std::string&& msg)
{
	_queue.push(msg);
}

bool SpStoreDBManager::waiting_swap_pending_queue(std::deque<std::string>& queue, unsigned long timeout)
{
	return _queue.waiting_swap(queue, timeout);
}

void SpStoreDBManager::errorSQL(DBPROCESS *dbproc, int severity, int dberr, int oserr, char *dberrstr, char *oserrstr)
{
	SPSTORE_ERROR("DB-LIBRARY ERREUR : " << dberrstr);

	if (oserr != DBNOERR)
	{
		SPSTORE_ERROR("Operating-system error: " << oserrstr );
	}
}
