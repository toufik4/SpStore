//*****************************************************************************
//
//  Copyright (c) Siemens Transportation Systems 2003
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version
//  associ� (.ver).
//
//  Fichier  : SpStoreDBMgr.cpp
//  Auteur   : Alexis KOLYBINE
//  Cr�ation : mardi 8 avril 2003
//  Contenu  : D�finition de la classe SpStoreDBMgr
//
//*****************************************************************************
//  Modifications : 
//*****************************************************************************
/*
#ifndef DB_ENGINE_ODBC
#define DB_ENGINE_ODBC
#endif
#ifndef DB_ENGINE_DBLIB
#define DB_ENGINE_DBLIB
#endif
*/
#include <SpStore/Interface/SpDB_ODBC.h>
#include <SpStore/Interface/SpDB_DBLib.h>
#include <SpStore/Interface/SpStoreDBMgr.h>
#include <SpStore/Interface/SpStoreTraces.h>
#include <SpStore/Interface/SpStoreRp.h>
#include <SpStore/Interface/SpDBEngine.h>



const SpCmnIntervalTime SpStoreDBMgr::_frozen_window(SpCmnIntervalTime::Milliseconds(120000));

SpStoreDBMgr* SpStoreDBMgr::_instance = 0;

/*
//.=================================================================
SpStoreDBMgr::t_IdRow::t_IdRow(SpStoreDBMgr* data_base) :
_data_base(data_base),
_bound(false)
{
}

//.=================================================================
SpStoreDBMgr::t_RestoreRow::t_RestoreRow(SpStoreDBMgr* data_base) :
t_IdRow(data_base)
{
}

bool SpStoreDBMgr::NextRow(SpStoreDBMgr::t_Row& row)
{ 
	bool bRet = false;
	if(_ptDBEngine)
		bRet = _ptDBEngine->NextRow(row);
	return bRet;
}

//.=================================================================
void SpStoreDBMgr::t_IdRow::bind()
{
    if (!_bound && _data_base->isGoodConnection()) {
		bindRegularResultColumns();
		_bound = true;
	}
	
}

//.=================================================================
void SpStoreDBMgr::t_IdRow::bindRegularResultColumns()
{
	//dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)_id); 
	_data_base->getDBEngine()->bind1(_id);
}

//.=================================================================
void SpStoreDBMgr::t_RestoreRow::bindRegularResultColumns()
{
	// t_IdRow::bindRegularResultColumns(dbproc);
	// dbbind (dbproc,2,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ATTR_LENGTH,(unsigned char *)_attr);
	// dbbind (dbproc,3,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_VALUE_LENGTH,(unsigned char *)_value);

	_data_base->getDBEngine()->bind2(_id,_attr,_value);
}
*/
SpStoreDBMgr::SpStoreDBMgr()
	: _is_initialized(false)
    , _loginName()
    , _password()
    , _processName()
    , _server()
    , _baseName()
    , _reconnectable(false)
   	, _last_connection_try(SpCmnDate::bad)
	, _queueHandler(new SpLogStore())
	, _ptDBEngine(0)
{
#ifdef DB_ENGINE_ODBC
	_ptDBEngine = new SpDB_ODBC();
#else
	_ptDBEngine = new SpDB_DBLib();
#endif // DB_ENGINE_ODBC
}

SpStoreDBMgr::~SpStoreDBMgr()
{
	delete _queueHandler;
	delete _ptDBEngine;
}

SpStoreDBMgr* SpStoreDBMgr::GetInstance()
{
    if (! _instance)
        _instance = new SpStoreDBMgr();

    return _instance;
}

void	SpStoreDBMgr::connect()
{
	_ptDBEngine->initConnect(_loginName.c_str(), _password.c_str(), _processName.c_str(), _server.c_str(), _baseName.c_str());
}

void SpStoreDBMgr::initConnection(const std::string& loginName, const std::string& loginPasswd,
	const std::string& processName, const std::string& serverName, const std::string& baseName, bool reconnectable)
{ 
	_loginName  = loginName;
   _password    = loginPasswd;
   _processName = processName;
   _server      = serverName;
   _baseName    = baseName;
   _reconnectable = reconnectable;

	if (!_is_initialized)
    {
		connect();
		_is_initialized = true; 
	}
}
/*
void SpStoreDBMgr::BeginTransaction()
{ 
	_ptDBEngine->BeginTransaction();
}

void SpStoreDBMgr::CommitTransaction()
{ 
	_ptDBEngine->CommitTransaction();
}

void SpStoreDBMgr::AddQuery(const IlsString& req)
{ 
	_ptDBEngine->AddQuery(req);
}

void SpStoreDBMgr::AddQuery(const std::string& req)
{ 
	_ptDBEngine->AddQuery(req);
}

void SpStoreDBMgr::ExecQuery()
{ 
	_ptDBEngine->ExecQuery();
}

void SpStoreDBMgr::CancelQuery()
{ 
	_ptDBEngine->CancelQuery();
}

bool SpStoreDBMgr::HasLastQuerySucced()
{ 
	return _ptDBEngine->HasLastQuerySucced();
}

bool SpStoreDBMgr::NoMoreResults()
{ 
	return _ptDBEngine->NoMoreResults(); 
}

bool SpStoreDBMgr::Failed()
{ 
	return _ptDBEngine->Failed();
}

bool SpStoreDBMgr::isGoodConnection() const {
	bool bRet = false;
	if(_ptDBEngine)
		bRet = _ptDBEngine->isGoodConnection();
	return bRet;
}
*/
void SpStoreDBMgr::closeConnection()
{ 
	_ptDBEngine->closeConnect(); 
	_is_initialized = false; 
}
 


bool	SpStoreDBMgr::CheckConnection() // getDbproc()
{
    // si reconnectable sur panne (store doit �tre reconnectable mais restore non : on ne peut bloquer l'init
    // du serveur si la machine d'archivage est en panne
    if (_reconnectable)
    {
        // si la connexion est morte 
        if (!_ptDBEngine->isGoodConnection())
        {
            // si le dernier essai de reconnexion date de plus de n secondes
			if (_last_connection_try.isBad() || (SpCmnDate::Now() - _last_connection_try  > _frozen_window))
            {
                // essai de reconnexion
                connect();
                _last_connection_try = SpCmnDate::Now();

                // si reconnexion OK
                if (_ptDBEngine->isGoodConnection())
                {
                    // prise en compte par la repr�sentation
                    SpStoreRepresentation::OnDBReconnection();
                }
            }
        }
    }
	return (_ptDBEngine->isGoodConnection());
}

void SpStoreDBMgr::asyncLog(std::string&& msg)
{
	_queue.push(msg);
}

bool SpStoreDBMgr::waiting_swap_pending_queue(std::deque<std::string>& queue, unsigned long timeout)
{
	return _queue.waiting_swap(queue, timeout);
}
