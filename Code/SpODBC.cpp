/***************************************************************************

    This file is part of project: SpODBC
    TinyODBC is hosted under: http://code.google.com/p/SpODBC/

    Copyright (c) 2008-2011 SqUe <squarious _at_ gmail _dot_ com>

    The MIT Licence

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*****************************************************************************/
//#define DB_ENGINE_ODBC
#ifdef DB_ENGINE_ODBC
#include <SpStore/Interface/SpODBC.h>
#include <string.h>
#include <memory>

// Macro for easy return code check
#define SPODBC_SUCCESS_CODE(rc) \
	((rc==SQL_SUCCESS)||(rc==SQL_SUCCESS_WITH_INFO))


namespace SpODBC
{
	// Current version
	unsigned short version_major()		{	return 1;	}
	unsigned short version_minor()		{	return 0;	}
	unsigned short version_revision()	{	return 0;	}

	//! @cond INTERNAL_FUNCTIONS

	// Get an error of an ODBC handle
	bool __get_error(SQLSMALLINT _handle_type, SQLHANDLE _handle, TString & _error_desc, TString & _status_code)
	{
		TCHAR status_code[256];
		TCHAR error_message[256];
		SQLINTEGER i_native_error;
		SQLSMALLINT total_bytes;
		RETCODE rc;

		// Ask for info
		rc = SQLGetDiagRec(
			_handle_type,
			_handle,
			1,
			(SQLTCHAR *)&status_code,
			&i_native_error,
			(SQLTCHAR *)&error_message,
			sizeof(error_message),
			&total_bytes);

		if (SPODBC_SUCCESS_CODE(rc))
		{
			_error_desc = error_message;
			_status_code = status_code;
			return true;
		}

		return false;
	}

	//! @endcond

	///////////////////////////////////////////////////////////////////////////////////
	// CONNECTION IMPLEMENTATION
	///////////////////////////////////////////////////////////////////////////////////

	//! @cond INTERNAL_FUNCTIONS

	// Allocate HENV and HDBC handles
	void __allocate_handle(HENV & _env, HDBC & _conn)
	{
		// Allocate enviroment
		SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &_env);

		/* We want ODBC 3 support */
		SQLSetEnvAttr(_env, SQL_ATTR_ODBC_VERSION, (SQLPOINTER *) SQL_OV_ODBC3, 0);

		// A Connection handle
		SQLAllocHandle(SQL_HANDLE_DBC, _env, &_conn);	
	}

	//! @endcond


	// Construct by data source
	Connection::Connection(const TString & _dsn,
				const TString & _user,
				const TString & _pass)
		:_hEnv(NULL),
		_hDbc(NULL),
		_bConnected(false)
	{
		
		// Allocate handles
		__allocate_handle(_hEnv, _hDbc);

		// open Connection too
		Connect(_dsn, _user, _pass);
	}

	// Default constructor
	Connection::Connection()
		:_hEnv(NULL),
		_hDbc(NULL),
		_bConnected(false)
	{
		// Allocate handles
		__allocate_handle(_hEnv, _hDbc);
	}
	
	// Destructor
	Connection::~Connection()
	{
		// close Connection
		Disconnect();

		// Close Connection handle
		SQLFreeHandle(SQL_HANDLE_DBC, _hDbc);

		// Close enviroment
		SQLFreeHandle(SQL_HANDLE_ENV, _hEnv);
	}

	// open a Connection with a data_source
	bool Connection::Connect(const TString & _dsn,	const TString & _user, const TString & _pass)
	{
		RETCODE rc;
		
		// Close if already open
		Disconnect();

		// Close previous Connection handle to be sure
		SQLFreeHandle(SQL_HANDLE_DBC, _hDbc);

		// Allocate a new Connection handle
		rc = SQLAllocHandle(SQL_HANDLE_DBC, _hEnv, &_hDbc);

		// Connect!
		rc = SQLConnect(_hDbc, 
			(SQLTCHAR *) _dsn.c_str(),
			SQL_NTS,
			(_user.size() > 0)?(SQLTCHAR *)_user.c_str():NULL,
			SQL_NTS,
			(_pass.size() > 0)?(SQLTCHAR *)_pass.c_str():NULL,
			SQL_NTS
			);

		if (!SPODBC_SUCCESS_CODE(rc))
			_bConnected = false;
		else
			_bConnected = true;

		return _bConnected;
	}

	// Check if it is open
	bool Connection::Connected() const
	{
		return _bConnected;
	}

	// Close Connection
	void Connection::Disconnect()
	{
		// Disconnect
		if (Connected())
			SQLDisconnect(_hDbc);

		_bConnected = false;
	}

	// Get last error description
	TString Connection::LastError()
	{
		TString error, state;
		
		// Get error message
		__get_error(SQL_HANDLE_DBC, _hDbc, error, state);

		return error;
	}

	// Get last error code
	TString Connection::LastErrorStatusCode()
	{
		TString error, state;
		
		__get_error(SQL_HANDLE_DBC, _hDbc, error, state);

		return state;
	}

	///////////////////////////////////////////////////////////////////////////////////
	// FIELD IMPLEMENTATION
	///////////////////////////////////////////////////////////////////////////////////

	//! @cond INTERNAL_FUNCTIONS

	template<class T>
	T __get_data(HSTMT _stmt, int _col, SQLSMALLINT _ttype, T error_value)
	{
		T tmp_storage;
		SQLINTEGER cb_needed;
		RETCODE rc;
		rc = SQLGetData(_stmt, _col, _ttype, &tmp_storage, sizeof(tmp_storage), &cb_needed);
		if (!SPODBC_SUCCESS_CODE(rc))
			return error_value;
		return tmp_storage;
	}

	//! @endcond

	// Not direct contructable
	Field::Field(HSTMT _stmt, int __colNum)
		:_hStmt(_stmt),
		_colNum(__colNum)
	{}

	//! Destructor
	Field::~Field()
	{}

	// Copy constructor
	Field::Field(const Field & r)
		:_hStmt(r._hStmt),
		_colNum(r._colNum)
	{
	}

	// Copy operator
	Field & Field::operator=(const Field & r)
	{
		_hStmt = r._hStmt;
		_colNum = r._colNum;
		return *this;
	}

	// Get field as string
	TString Field::as_string() const
	{
		SQLINTEGER sz_needed = 0;
		TCHAR small_buff[256];
		RETCODE rc;
				
		// Try with small buffer
		rc = SQLGetData(_hStmt, _colNum, SQL_C_TCHAR, small_buff, sizeof(small_buff), &sz_needed);
		
		if (SPODBC_SUCCESS_CODE(rc))
			return TString(small_buff);
		else if (sz_needed > 0)
		{	// A bigger buffer is needed
			SQLINTEGER sz_buff = sz_needed + 1;
			std::auto_ptr<TCHAR> p_data(new TCHAR[sz_buff]);
			SQLGetData(_hStmt, _colNum, SQL_C_TCHAR, (SQLTCHAR *)p_data.get(), sz_buff, &sz_needed);
			return TString(p_data.get());
		}

		return TString();	// Empty
	}

	// Get field as long
	long Field::as_long() const
	{
		return __get_data<long>(_hStmt, _colNum, SQL_C_SLONG, 0);
	}

	// Get field as unsigned long
	unsigned long Field::as_unsigned_long() const
	{
		return __get_data<unsigned long>(_hStmt, _colNum, SQL_C_ULONG, 0);
	}

	// Get field as double
	double Field::as_double() const
	{
		return __get_data<double>(_hStmt, _colNum, SQL_C_DOUBLE, 0);
	}

	// Get field as float
	float Field::as_float() const
	{
		return __get_data<float>(_hStmt, _colNum, SQL_C_FLOAT, 0);
	}

	// Get field as short
	short Field::as_short() const
	{
		return __get_data<short>(_hStmt, _colNum, SQL_C_SSHORT, 0);
	}

	// Get field as unsigned short
	unsigned short Field::as_unsigned_short() const
	{
		return __get_data<unsigned short>(_hStmt, _colNum, SQL_C_USHORT, 0);
	}

	///////////////////////////////////////////////////////////////////////////////////
	// PARAM IMPLEMENTATION
	///////////////////////////////////////////////////////////////////////////////////

	//! @cond INTERNAL_FUNCTIONS
	template <class T>
	const T & __bind_param(HSTMT _stmt, int _parnum, SQLSMALLINT _ctype, SQLSMALLINT _sqltype, void * dst_buf, const T & _value)
	{
		// Save buffer internally
		memcpy(dst_buf, &_value, sizeof(_value));

		// Bind on internal buffer		
		SQLINTEGER StrLenOrInPoint = 0;
		SQLBindParameter(_stmt,
			_parnum,
			SQL_PARAM_INPUT,
			_ctype,
			_sqltype,
			0,
			0,
			(SQLPOINTER *)dst_buf,
			0,
			&StrLenOrInPoint
			);

		return *(T *) dst_buf;
	}

	//! @endcond

	// Constructor
	Param::Param(HSTMT _stmt, int __parNum)
		:_hStmt(_stmt),
		_parNum(__parNum)
	{}

	// Copy constructor
	Param::Param(const Param & r)
		:_hStmt(r._hStmt),
		_parNum(r._parNum)
	{}

	// Destructor
	Param::~Param()
	{}

	// Copy operator
	Param & Param::operator=(const Param & r)
	{
		_hStmt = r._hStmt;
		_parNum = r._parNum;
		return *this;
	}

	// Set as string
	const TString & Param::set_as_string(const TString & _str)
	{
		// Save buffer internally
		_int_string = _str;

		// Bind on internal buffer		
		_int_SLOIP = SQL_NTS;
		SQLBindParameter(_hStmt,
			_parNum,
			SQL_PARAM_INPUT,
			SQL_C_TCHAR,
			SQL_CHAR,
			(SQLUINTEGER)_int_string.size(),
			0,
			(SQLPOINTER *)_int_string.c_str(),
			(SQLINTEGER)_int_string.size()+1,
			&_int_SLOIP);;

		return _int_string;
	}

	// Set as string
	const long & Param::set_as_long(const long & _value)
	{
		return __bind_param(_hStmt, _parNum, SQL_C_SLONG, SQL_INTEGER, _int_buffer, _value);
	}

	// Set parameter as usigned long
	const unsigned long & Param::set_as_unsigned_long(const unsigned long & _value)
	{
		return __bind_param(_hStmt, _parNum, SQL_C_ULONG, SQL_INTEGER, _int_buffer, _value);
	}

	///////////////////////////////////////////////////////////////////////////////////
	// STATEMENT IMPLEMENTATION
	///////////////////////////////////////////////////////////////////////////////////

	// Default constructor
	Command::Command()
		:_hStmt(NULL),
		_bOpen(false)
	{
	}

	// Construct and initialize
	Command::Command(Connection & _conn, const TString & _stmt)
		:_hStmt(NULL),
		_bOpen(false)
	{
		prepare(_conn, _stmt);
	}

	// Destructor
	Command::~Command()
	{
		close();
	}

	// Used to create a Command (used automatically by the other functions)
	bool Command::open(Connection & _conn)
	{
		RETCODE rc;
		// close previous one
		close();

		// Allocate Command
		rc = SQLAllocHandle(SQL_HANDLE_STMT, _conn.dbcHandle(), &_hStmt);
		if (!SPODBC_SUCCESS_CODE(rc))
		{
			_hStmt = NULL;
			_bOpen = false;
			return false;
		}

		_bOpen = true;
		return true;
	}

	// Check if it is an open Command
	bool Command::is_open() const
	{
		return _bOpen;
	}

	// Close Command
	void Command::close()
	{
		if (is_open())
		{
			// Free parameters
			param_it it;
			for(it = m_params.begin();it != m_params.end();it++)
				delete it->second;
			m_params.clear();

			// Free result if any
			free_results();

			// Free handle
			SQLFreeHandle(SQL_HANDLE_STMT, _hStmt);
			_hStmt = NULL;
		}
		_bOpen = false;
	}

	// Free results (aka SQLCloseCursor)
	void Command::free_results()
	{
		// Close cursor if we have an open Connection
		if (is_open())
			SQLCloseCursor(_hStmt);
	}

	// Prepare Command
	bool Command::prepare(Connection & _conn, const TString & _stmt)
	{
		RETCODE rc;
		// Close previous
		close();

		// open a new one
		if (!open(_conn))
			return false;

		// Prepare Command
		rc = SQLPrepare(_hStmt, (SQLTCHAR *)_stmt.c_str(), SQL_NTS);

		if (!SPODBC_SUCCESS_CODE(rc))
			return false;

		return true;
	}


	// Execute direct a query
	bool Command::ExecuteDirect(Connection & _conn, const TString & _query)
	{
		RETCODE rc;
		// Close previous
		close();

		// open a new one
		if (!open(_conn))
			return false;

		// Execute directly Command
		rc = SQLExecDirect(_hStmt, (SQLTCHAR *)_query.c_str(), SQL_NTS);
		if (!SPODBC_SUCCESS_CODE(rc))
			return false;

		return true;
	}

	// Execute Command
	bool Command::Execute()
	{
		RETCODE rc;
		if (!is_open())
			return false;

		rc = SQLExecute(_hStmt);
		if (!SPODBC_SUCCESS_CODE(rc))
			return false;
		return true;
	}

	// Fetch next
	bool Command::FetchNext()
	{
		RETCODE rc;
		if (!is_open())
			return false;

		rc = SQLFetch(_hStmt);
		if (SPODBC_SUCCESS_CODE(rc))
			return true;
		return false;
	}

	// Get a field by column number (1-based)
	const Field Command::field(int _num) const
	{	
		return Field(_hStmt, _num);
	}

	// Count columns of the result
	int Command::count_columns() const
	{
		SQLSMALLINT _total_cols;
		RETCODE rc;

		if (!is_open())
			return -1;

		rc = SQLNumResultCols(_hStmt, &_total_cols);
		if (!SPODBC_SUCCESS_CODE(rc))
			return -1;
		return _total_cols;
	}

	// Get last error description
	TString Command::LastError()
	{
		TString error, state;
		
		// Get error message
		__get_error(SQL_HANDLE_STMT, _hStmt, error, state);

		return error;
	}

	// Get last error code
	TString Command::LastErrorStatusCode()
	{
		TString error, state;
		
		__get_error(SQL_HANDLE_STMT, _hStmt, error, state);

		return state;
	}

	// Handle a parameter
	Param & Command::param(int _num)
	{
		// Add a new if there isn't one
		if (0 == m_params.count(_num))
			m_params[_num] = new Param(_hStmt, _num);
		
		return *m_params[_num];
	}

	// Reset parameters (unbind all parameters
	void Command::reset_parameters()
	{
		if (!is_open())
			return;

		SQLFreeStmt(_hStmt, SQL_RESET_PARAMS);
	}

};	// !namespace SpODBC

#endif // DB_ENGINE_ODBC