//*****************************************************************************
//
//  Copyright (c) Siemens Transportation Systems 2003
//  Ce fichier est juridiquement protégé par la loi 98-531 du 1er juillet 1998
//  La version et les évolutions sont identifiées dans le fichier de version
//  associé (.ver).
//
//  Fichier  : SpLogStore.cpp
//  Auteur   : Philippe HOUDEBINE
//  Création : jeudi 17 mai 2018
//  Contenu  : Définition de la classe SpLogStore
//
//*****************************************************************************
#include <SpStore/Interface/SpLogStore.h>
#include <SpStore/Interface/SpStoreRp.h>
#include <SpStore/Interface/SpStoreTraces.h>


SpLogStore::SpLogStore()
{
	SPSTORE_INFO("Start storing thread.");
	resume();
}

SpLogStore::~SpLogStore()
{
	interrupt();
	join();
	SPSTORE_INFO("Storing thread terminated.");
}

unsigned long SpLogStore::do_run()
{
	SpStoreRepresentation::DoStore(INFINITE);
	SPSTORE_WARNING("Storing thread left its main loop. It is terminated before destruction of DBManager.");
	return 0UL;
}

void SpLogStore::on_abort(unsigned exit_code)
{
	SPSTORE_FATAL_ERROR("Thread aborted with exit code " + exit_code);
}
