
#ifdef DB_ENGINE_ODBC
#include <SpStore/Interface/SpODBC.h>
#include <iostream>

using namespace std;

int main_3(int argc, char **argv)
{   
    SpODBC::Connection my_Connection;
    SpODBC::Command my_Command;

    // Create a Connection with an ODBC Data Source
    if (!my_Connection.connect("MyServer", "", ""))
    {
        cout << "Cannot connect to the Data Source" << endl
            << my_Connection.LastError();
        return 1;
    }

    // Execute a direct query
    if (!my_Command.ExecuteDirect(my_Connection,
        "INSERT INTO books (id, title, author) VALUES(\'0\', \'SpODBC Manual\', \'sque\')"))
    {
        cout << "Cannot execute query!" << endl
            << my_Connection.LastError();
        return 2;
    }

    return 0;
}
#endif // DB_ENGINE_ODBC