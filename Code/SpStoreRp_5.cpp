//*****************************************************************************
//
//  Copyright (c) Matra Transport International 2001
//  Ce fichier est juridiquement prot�g� par la loi 98-531 du 1er juillet 1998
//  La version et les �volutions sont identifi�es dans le fichier de version
//  associ� (.ver).
//
//  Fichier  : SpStoreRp.cpp
//  Auteur   : Alexis Kolybine
//  Cr�ation : lundi 18 juin 2001
//  Contenu  : D�finition de la classe SpStoreRp
//
//*****************************************************************************
//  Modifications : 
//*****************************************************************************
#define SPSTORE_LOG_DEBUG

#include <SpStore/Interface/SpStoreRp.h>
#include <SpStore/Interface/SpStoreTraces.h>
#include <SpStore/Interface/SpStoreDBMgr.h> // TAB SpStoreDBManager -> SpStoreDBMgr
//#include <SpStore/Interface/SpDB_DBLib.h>
#include <SpStore/Interface/SpDBEngine.h>

#include <SpCmn/Interface/SpCmnAccess.h>
#include <SpCmn/Interface/SpCmnDate.h>
#include <SpCmn/Interface/SpCmnMvValueMap.h>
#include <SpCmn/Interface/SpCmnMvValueVector.h>
#include <SpCmn/Interface/SpCmnDirectoryService.h>
#include <SpCmn/Interface/SpCmnParamAppli.h>

#include <XtOsi/Interface/XtOsiStringAlgorithm.h>

#include <ciso646>
#include <sstream>

/// -----------------------------------------------------------------------------------------------
namespace
{
	std::string SQL_string_delimiter					= "'";
	std::string SQL_string_delimiter_escape_sequence	= "''";

	// indique pour le restore que l'objet est � cr�er dynamiquement.
	IlsString IS_DYNAMIC = "IS_DYNAMIC";

	// indique le nom de la classe cot� serveur pour la construction dynamique de l'objet.
	IlsString SV_TYPE = "SV_TYPE";
}

SpStoreDummy_ToForceLink::SpStoreDummy_ToForceLink() {;}

// flag base de donn�e d�connect�e et non encore reconnect�e compl�tement
xtl::threading::synchronized_value<bool> SpStoreRepresentation::_isDBDisconnection = false;

// reconnexion avec le serveur
bool SpStoreRepresentation::_isReconnecting = false;

// representation utilis�e pour la d�connexion serveur
SpStoreRepresentation* SpStoreRepresentation::_representationActive = 0;

int SpStoreQPNC = 50 ; // lu en param�tre dans la ligne de commande

//// ----------------------------------------------------------------------------------------------
//// Operation: setHost : SpStoreRepresentation
//
//// Semantics:
//   recherche du nom du serveur SQL 
//// ----------------------------------------------------------------------------------------------
void SpStoreRepresentation::setHost(const IlsString& host)
{ 
	if (host[0] == '%') { // si c'est une variable d'evt
		IlsString real_host = getenv(host.substring(1, host.getLength() - 2).getValue());
		if (real_host != "")
			_host = real_host;
		else {
			SPSTORE_FATAL_ERROR(host.substring(1, host.getLength() - 2).getValue() << " is unkown enviroment variable");
			exit(2);
		}

	} else {
		_host = host; 
	}
}


//// ----------------------------------------------------------------------------------------------
//// Operation: setId : SpStoreRp
//
//// Semantics:
//   M�morisation de l'identificateur et 
//   ajout de l'objet dans la liste des objets � storer :_stored_objects
//// ----------------------------------------------------------------------------------------------
void SpStoreRp::setId(const IlsString &id)
{ 
	_id  = id;
	static_cast<SpStoreRepresentation*>(getRepresentation())->addObject(this);
}

//// ----------------------------------------------------------------------------------------------
//// Operation: store : SpStoreRp
//
//// Semantics:
//   construction de la requ�te SQL de mise � jour et  m�morisation 
//   dans _request_queue. La requ�te sera ex�cut�e par DoStore
//   Tous les termes sont d�j� cr��s dans la BD lors de l'init
//// ----------------------------------------------------------------------------------------------
void SpStoreRp::store(const IlsString& attr, const IlsMvValue& value)
{
	//Stockage de la derni�re valeur de l'attribut notifi�
	_rpValues[attr] = value.asString();

    if (!SpStoreDBMgr::GetInstance()->isInitialized()) // TAB SpStoreDBManager -> SpStoreDBMgr
    {
		return;
    }

    if (!_id.isEmpty())
    {
        SpStoreRepresentation* rep = dynamic_cast<SpStoreRepresentation*>(getRepresentation());

        // lors d'une reconnexion, store effectu� que si la valeur a �t� modifi�e pendant la phase 
        // de d�connexion
        // Remarque : la variable rep est si possible non utilis�e en phase de d�connexion serveur
        // car elle peut �tre incoh�rente (c�d plant�) => variable statique isReconnecting et isDBDisconnection
        if (! SpStoreRepresentation::IsReconnecting())
        {
            // au d�marrage, les valeurs ne sont "stor�es"
            if (! rep || !rep->isCreated())
            {
                SPSTORE_DEBUG("cr�ation storeRp pour : " << _id);
		        return;
            }

        }

        std::ostringstream req;

        IlsString _table = rep->getTable();

		req << "update " << _table.getValue() 
			<< " set value = '" << SpStoreRp::getSQLCompliantString(value)
			<< "', date = '" << SpCmnDate()
			<< "' where (id = '" << _id << "' AND attr = '" << attr << "')";
		SpStoreDBMgr::GetInstance()->asyncLog(req.str()); // TAB SpStoreDBManager -> SpStoreDBMgr
    }
}

//// ----------------------------------------------------------------------------------------------
//// Operation: getSQLCompliantString : SpStoreRepresentation
//
//// Semantics:
//   Renvoie une cha�ne de caract�res dont toutes les simples quotes ont �t� remplac�es par des doubles quotes.
//// ----------------------------------------------------------------------------------------------
std::string SpStoreRp::getSQLCompliantString(const IlsMvValue& value)
{
	if (value.isString())
	{
		std::string str(value.asString().value());

		xtosi::replace_all(str, SQL_string_delimiter, SQL_string_delimiter_escape_sequence);

		return str;
	}
	
	return value.asString().value();
}

//.=================================================================
//.Class : SpStoreRepresentation
//.----- 
//. 
//.Member Function : SpStoreRepresentation
//.--------------- 
//. 
//.=================================================================
SpStoreRepresentation::SpStoreRepresentation(IlsMvComponent& c, const IlsRpModel& m)
    : IlsRepresentation(c,m)
    , _is_created(false)
    , _host()
    , _schema()
    , _table()
{
	SPSTORE_INFO("Construction of SpStoreRepresentation");
    const SpCmnParamAppli& properties = SpCmnParamAppli::getInstance();
    _host   = properties.getStringValue("store.database.hostname", "").c_str();
    _schema = properties.getStringValue("store.database.schema", "").c_str();
    _table  = properties.getStringValue("store.database.table", "").c_str();
}

//------------------------------------------------------------------------------
SpStoreRepresentation::~SpStoreRepresentation()
{
	SPSTORE_INFO("Destruction of SpStoreRepresentation");
    if (! _isDBDisconnection)
    {
    	// Traitement d'eventuelles requetes en attente.
		DoStore(0);

		SpStoreDBMgr::GetInstance()->closeConnection(); // TAB SpStoreDBManager -> SpStoreDBMgr
	}
}

//.=================================================================
//// Operation: addObject : SpStoreRepresentation
//
//// Semantics:
//. ajout d'objet dans stored_object pour pouvoir ajouter les termes manquant sous SQL
//. 
//.=================================================================
void SpStoreRepresentation::addObject(SpStoreRp* rp)
{
    const IlsRpObjModel* model = rp->getModel();
    IlsRpAttributeId nb_attr = static_cast<IlsRpAttributeId>(model->getNbAttrs());

    for (IlsRpAttributeId i_attr = 1; i_attr <= nb_attr; i_attr++)
    {
	    IlsString attr = model->getAttrModel(i_attr)->getLabel();

        if (attr != "replicServerId" && attr != "id")
		{    
            StoreId key(rp->getId(), attr); 
            _stored_objects[key] = rp;
        }
    }
}

//// ----------------------------------------------------------------------------------------------
//// Operation: DoStore : SpStoreRepresentation
//
//// Semantics:
//   Ex�cution des requ�tes m�moris�es dans la file partag�e des requ�tes, par transactions de
//   SpStoreQPNC requ�tes au maximum.
//// ----------------------------------------------------------------------------------------------
void SpStoreRepresentation::DoStore(const unsigned long timeout)
{
	SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance(); // TAB SpStoreDBManager -> SpStoreDBMgr
	std::deque<std::string> message_queue; // pile de deuxi�me niveau

	
    // permutation des piles avec contr�le des conflits d'�criture
	while (data_base->waiting_swap_pending_queue(message_queue, timeout))
	{
		
	    // si il y a eu d�connection SQL - positionnement de l'�tat d�connexion neutralisant la cr�ation de
	    // nouvelle requ�te et le stockage dans la BD store
		if (! data_base->getDBEngine()->isGoodConnection() && ! _isDBDisconnection)
	    {
            SPSTORE_INFO("DECONNEXION BD");
			_isDBDisconnection = true;
	    }
	
	    // for�age de la reconnexion SQL s'il y a eu d�connexion
	    // DBPROCESS* dbproc = data_base->getDbproc(); // TAB

	
	    if (data_base->CheckConnection() && ! _isDBDisconnection)
	    {
	        // boucle de lecture de toutes les requ�tes m�moris�es et 
		    // ajout dans la string qui sera ex�cut�e dans la transaction
			while(!message_queue.empty())
			{
				// prevents from executing transactions with more than SpStoreQPNC transactions (too long transactions may cause issues)
				int i = 1;
				while ((i < SpStoreQPNC) && (!message_queue.empty()))
				{
					const std::string&	request = message_queue.front();
					data_base->AddQuery(request); // TAB AddQuery
					SPSTORE_DEBUG("Add request " << i << " : " << request);
					message_queue.pop_front();
					i++;
				}
	
				// ex�cution des requ�tes dans SQL 
				data_base->ExecQuery(); // TAB ExecQuery

				// int code_retour; // TAB
				int reqCounter = 1;
				while (!data_base->NoMoreResults())
				{
					if (data_base->Failed())
					{
						SPSTORE_ERROR("Insertion de message KO : Request " << reqCounter << " failed. Abort checking the requests' results.");
						break;
					}
					reqCounter++;
				}
			}
	    }
		else if(!message_queue.empty())
		{
			// if the connexion is bad, we drop the non executed requests
			SPSTORE_DEBUG("Bad SQL connexion : dropping " << message_queue.size() << " requests !");
			message_queue.clear(); 
		}
	}
}

//// ----------------------------------------------------------------------------------------------
//// Operation: beginS2CTransaction : SpStoreRepresentation
//
//// Semantics:
//   traitement ex�cut� sur le d�but de la notif
//// ----------------------------------------------------------------------------------------------
IlsBoolean SpStoreRepresentation::beginS2CTransaction(IlsS2CTransStatus transStatus,
                               IlsTransactionId /*tid*/)
{
    if (transStatus == ILS_S2C_NOTIFY_CREATION)
    {
        _representationActive = this;

        if (_isDBDisconnection)
        {
            SPSTORE_INFO("Store RECONNEXION");
            _isReconnecting = true;
        }
    }
	return IlsTrue;
}

//// ----------------------------------------------------------------------------------------------
//// Operation: initialiseTable : SpStoreRepresentation
//
//// Semantics:
//   Initialisation de la table store avec cr�ation de toutes les lignes associ�es
//   aux diff�rents rp � storer. 
//
//   Si les objets sont d�j� dans la BD, on ne fait rien, sinon , on ins�re les lignes 
//   pour tous les attributs dans la BD 
//// ----------------------------------------------------------------------------------------------
void SpStoreRepresentation::initialiseTable()
{
	IlsString DELETE_ALL_REQUEST = IlsString("delete from ") << _table;
	IlsString SELECT_ALL_REQUEST = IlsString("select id, attr, value from ") << _table;

	SpStoreDBMgr*			data_base = SpStoreDBMgr::GetInstance(); 

	// ex�cution de la requ�te de s�lection de toutes les lignes de la table store 
    // DBPROCESS* dbproc = data_base->getDbproc(); // TAB
    if (data_base->CheckConnection())
    {
        const SpCmnParamAppli& properties = SpCmnParamAppli::getInstance();
        if (properties.getBooleanValue("store.database.delete_rows_on_initialize_table", false))
        {
            data_base->AddQuery(DELETE_ALL_REQUEST);
            data_base->ExecQuery(); // TAB ExecQuery
            if (not data_base->HasLastQuerySucced())
            {
                // erreur d'ex�cution du delete
                SPSTORE_FATAL_ERROR("Unable to execute '" << DELETE_ALL_REQUEST << "'");
                return;
            }
        }

	    data_base->AddQuery(SELECT_ALL_REQUEST);	
	    data_base->ExecQuery(); // TAB ExecQuery
	    if (!data_base->HasLastQuerySucced())
        {
			// erreur d'ex�cution du select 
		    SPSTORE_FATAL_ERROR("Unable to execute '" << SELECT_ALL_REQUEST << "'");
		    return;
	    }

		// d�pouillement de toutes les lignes lues dans la BD : m�moris�es dans data_base 
	    std::map<StoreId, SpStoreRp*>::iterator i;

	    // SpDB_DBLib::t_RestoreRow row(data_base->getDBEngine()); /// TAB Row
		SpStoreDBMgr::t_RestoreRow row(data_base); /// TAB2

	    //while (data_base->getDBEngine()->NextRow(row))
		while (data_base->NextRow(row)) // TAB2
        {
			// pour chaque ligne de la BD recherche si l'id est m�moris� dans 
			// la liste des objets � storer : _stored_objects 
            StoreId id(row.getId(), row.getAttr());
		    i = _stored_objects.find(id);

		    if ( i != _stored_objects.end() )
            {
				// OUI : objet devant �tre stor� et d�j� dans la BD
				// on ne doit donc pas faire d'insert, on le supprime donc
				// de la liste des objets pour lesquels ont doit faire un insert 
//			    SpStoreRp* rp = (*i).second;
//				IlsString attr = row.getAttr();
//			    if (rp->getAttributeId(attr) != -1)
//                {
					// l'attribut est d�j� m�moris� => suppression de la liste
                    SPSTORE_DEBUG("Pas d'insertion pour : Id (" << row.getId() << "," << row.getAttr() << ")");
				    _stored_objects.erase(id);
//			    }
		    }
	    }

	    data_base->CancelQuery();

        // Cr�ation des �lements qui ne sont pas encore dans la table, initialis�s � leur valeur courante
		//renvoy�e par le serveur.
	    SPSTORE_INFO("Creation of " << _stored_objects.size() << " empty lines");
		
		if (_stored_objects.size() > 0)
		{
			std::string s_union;
			std::string s_queries;

			int queries = 0, lines = 0;

			// un d�passement de 50000 char pour la requete SQL provoque son flush
			// Ce seuil est empirique
			const int MIN_FLUSH_QUERY_SIZE = 50000;

			for(i = _stored_objects.begin(); i != _stored_objects.end(); ++i)
			{
				lines++;
				if (queries == 0)
					data_base->AddQuery(IlsString("insert into ") <<  getTable().value() << " (date, id, attr, value) ");

				queries++;

				StoreId key = (*i).first;
                IlsString id   = key.first;
                IlsString attr = key.second;

                SpStoreRp* rp = (*i).second;
				
/*
                const IlsRpObjModel* model = rp->getModel();
				int nb_atr = model->getNbAttrs();

				for (int i_atr = 1; i_atr <= nb_atr; i_atr++)
				{
					IlsString atr = model->getAttrModel(i_atr)->getLabel();

					if (atr != "replicServerId" && atr != "id")
					{
               */
						std::ostringstream req;
						req << s_union << "select '" 
							<< SpCmnDate()  << "', '" << id << "', '"
							<< attr.value() << "', '" << rp->getAttributeValue(attr)  << "' ";
						s_union = "union ";

						data_base->AddQuery(req.str());
						s_queries += req.str();
//					}


//				}
				//Si la taille de la requ�te d�passe le seuil de flush OU que l'on a trait�
				// l'ensemble des donn�es ALORS on flush la requ�te au serveur SQL.
				if (s_queries.size() > MIN_FLUSH_QUERY_SIZE || lines == _stored_objects.size())
				{
					// ex�cution des requ�tes dans SQL 
					SPSTORE_INFO("Insertion of " << queries << " lines of " << s_queries.size() << " char");
					s_queries = "";
					data_base->ExecQuery(); // TAB ExecQuery
					//int code_retour; // TAB
					while (!data_base->NoMoreResults())
					{
						if (data_base->Failed())
						{
							SPSTORE_ERROR("Insertion de message KO");
							return;
						}
					}
					queries = 0;
					s_union = "";
				}

			}

		}
      	SPSTORE_NOTE("Fin Init BD Store - SQL");      
	}
}

//// ----------------------------------------------------------------------------------------------
//// Operation: OnDBReconnection : SpStoreRepresentation
//
//// Semantics:
//		D�connexion avec le serveur pour forcer une notification g�n�rale
//      et ainsi r�cup�r� les valeurs modifi�es durant la d�connexion BD
//.=================================================================
void SpStoreRepresentation::OnDBReconnection()
{
    SPSTORE_INFO("FORCAGE DECONNEXION BD");
    _isDBDisconnection = true; // si pas encore positionn�

	std::deque<std::string> request_queue; // pile de deuxi�me niveau
	
    // permutation des piles avec contr�le des conflits d'�criture, sans attendre que la file soit non vide
	SpStoreDBMgr::GetInstance()->waiting_swap_pending_queue(request_queue, 0);
    
    while (!request_queue.empty()) // vidage de la file d'attente
    {
        SPSTORE_DEBUG("Requ�te : " << request_queue.front() << " supprim�");
        request_queue.pop_front();
    }

    if (_representationActive && _representationActive->getComponent())
	{
        SpReconComponent* comp = dynamic_cast<SpReconComponent*>(_representationActive->getComponent());
        SPSTORE_ERRORIF(comp==0, "SpReconComponent invalide");

        if (comp)
        {
            // for�age d�connexion serveur -> provoquera une reconnexion quelques seconde plus tard
            // avec destruction des Rp puis cr�ation de nouveau Rp
            comp->onDisconnect();
        }
    }
}

//// ----------------------------------------------------------------------------------------------
//// Operation: endS2CTransaction : SpStoreRepresentation
//
//// Semantics:
//   traitement sur fin de notification 
//// ----------------------------------------------------------------------------------------------
void SpStoreRepresentation::endS2CTransaction(IlsS2CTransStatus transStatus, IlsTransactionId transid)
{
	if (transStatus != ILS_S2C_NOTIFY_CREATION) {
		SPSTORE_DEBUG("end transaction");
	}
	else
	{
		namespace ds = spocc::directory_service;
		// sur premier appel , ouverture de la connection SQL 
		// et cr�ation de toutes les instances dans store si n�cessaire
        if (! _isDBDisconnection)
        {
			ds::user_account	account = ds::directory::instance().query_database_connection("spocc.store");
			SpStoreDBMgr::GetInstance()->initConnection(account.user(), account.password(), // TAB SpStoreDBManager -> SpStoreDBMgr
				"SpStore", _host.value(), _schema.value(), true);

	    	initialiseTable();
		    SPSTORE_INFO("begin storing on "
			    << _table.getValue() << "." << _schema.getValue() << "@" << _host);
        }
        SPSTORE_INFO("BD et SERVEUR CONNECTES");
        _isDBDisconnection = false;
        _isReconnecting = false;
	}
    // prise en compte des ordres store
    _is_created = true;
	IlsRepresentation::endS2CTransaction(transStatus, transid);
}


//// ----------------------------------------------------------------------------------------------
//// Operation: isInDynList : SpStoreRepresentation
//
//// Semantics:
//   TODO_inserer_ici_votre_algorithme.
//// ----------------------------------------------------------------------------------------------
IlsBoolean SpStoreRepresentation::isInDynList(const IlsString& id, const IlsString& attr)
{
	IlsBoolean result(IlsFalse);

	IlsString elt(id);
	elt << "::";
	elt << attr;

	std::list<std::string>::iterator it_list;

	for (it_list = _dyn_id_attribut_list.begin(); it_list != _dyn_id_attribut_list.end(); ++it_list)
	{
		if ( strcmp( (*it_list).c_str(), elt.value() ) == 0 )
		{
			result = IlsTrue;
			break;
		}
	}

	return result;
}


//// ----------------------------------------------------------------------------------------------
//// Operation: addDynList : SpStoreRepresentation
//
//// Semantics:
//   TODO_inserer_ici_votre_algorithme.
//// ----------------------------------------------------------------------------------------------
void SpStoreRepresentation::addDynList(const IlsString& id, const IlsString& attr)
{
	IlsString elt(id);
	elt << "::";
	elt << attr;

	_dyn_id_attribut_list.push_back(elt.value());
}


//// ----------------------------------------------------------------------------------------------
//// ----------------------------------------------------------------------------------------------

//// ----------------------------------------------------------------------------------------------
//// Operation: ~SpStoreDynRp : SpStoreDynRp
//
//// Semantics:
//   TODO_inserer_ici_votre_algorithme.
//// ----------------------------------------------------------------------------------------------
SpStoreDynRp::~SpStoreDynRp()
{
	SpStoreRepresentation* rep = dynamic_cast<SpStoreRepresentation*>(getRepresentation());

	if ( (rep != 0) && rep->isCreated() && SpStoreDBMgr::GetInstance()->isInitialized() ) // TAB SpStoreDBManager -> SpStoreDBMgr
	{
		// Alors :
		//    - la vue n'est pas ferm�e     
		//    - c'est une destruction explicite de l'objet serveur
		deleteRowAndChild( _id, rep->getTable() );
	}
}

//// ----------------------------------------------------------------------------------------------
//// Operation: deleteRowAndChild : SpStoreDynRp
//
//// Semantics:
//   Suppression en base de donn�es des enregistrements associ�s � l'objets et aussi ceux
//   des objets contenus par celui-ci.
//// ----------------------------------------------------------------------------------------------
void SpStoreDynRp::deleteRowAndChild(const IlsString& rowId, const IlsString& table)
{
	SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance(); // TAB SpStoreDBManager -> SpStoreDBMgr

	// Recherche des id associ�s � d'eventuels objets contenus.
	std::ostringstream req_sel;
	req_sel << "select id from " << table.value() 
		<< " where ( attr = 'HAS_OWNER' and value = '" 
		<< rowId.value() << "' )";

    // DBPROCESS* dbproc = data_base->getDbproc(); // TAB
    if (data_base->CheckConnection())
    {
	    data_base->AddQuery(req_sel.str());	
	    data_base->ExecQuery(); // TAB ExecQuery
    }

	if (!data_base->HasLastQuerySucced())
	{
		// Echec ou pas de resultat correspondant.
		SPSTORE_FATAL_ERROR("Unable to execute '" << req_sel.str() << "'");
	}
	else
	{
		// SpDB_DBLib::t_IdRow row_child(data_base->getDBEngine()); // TAB SpStoreDBManager -> SpStoreDBMgr
		SpStoreDBMgr::t_IdRow row_child(data_base); // TAB2

		// Pour chaque objet contenu
		// while (data_base->getDBEngine()->NextRow(row_child))
		while (data_base->NextRow(row_child)) // TAB2
		{
			IlsString childRowId = row_child.getId();
			// Suppression de l'objet contenu et de ses propres objets contenus (2ieme niveau)
			deleteRowAndChild( childRowId, table );
		}
	}

	// Suppression des enregistrements de l'objet.
	std::ostringstream req_del;
	req_del << "delete from " << table.value() << " where id = '" << rowId.value() << "'";

	data_base->asyncLog( req_del.str() );
}


//// ----------------------------------------------------------------------------------------------
//// Operation: store : SpStoreDynRp
//
//// Semantics:
//   M�thode appel�e sur mise � jour d'un attribut du Rp.
//// ----------------------------------------------------------------------------------------------
void SpStoreDynRp::store(const IlsString& attr, const IlsMvValue& value)
{
    // pendant la phase de d�connexion (et reconnexion non en cours) : store abandonn�
    if (! SpStoreRepresentation::IsReconnecting() && SpStoreRepresentation::IsDBDisconnection())
    {
         SPSTORE_WARNING("Store d�connect� pour : " << _id);
         return;
    }

    SpStoreRepresentation* rep = dynamic_cast<SpStoreRepresentation*>(getRepresentation());

	SpStoreDBMgr* data_base = SpStoreDBMgr::GetInstance(); // TAB SpStoreDBManager -> SpStoreDBMgr

	// Conditions n�cessaires
	if ( (rep == 0) || !rep->isCreated() || !data_base->isInitialized() )
	{
		return;
	}

    // Attributs a ne pas traiter.
	if (attr == "replicServerId" || attr == "id")
	{
		return;
	}

	IlsString _table = rep->getTable();
					
	// Cr�ation des attributs sp�cifiques aux objets dynamiques.
	// Si l'attribut 'IS_DYNAMIC' n'a pas ete cree.
	if ( !rep->isInDynList( _id, IS_DYNAMIC ) )
	{
		// Alors on l'ajoute a la liste des elements inseres.
		rep->addDynList( _id, IS_DYNAMIC );

		// Insertion de l'enregistrement caracterisant l'object comme etant dynamique.
		std::ostringstream req_dyn;
		// On v�rifie l'existence de cet enregistrement pour ne pas violer la contrainte de clef primaire
		req_dyn << "if exists(select * from " << _table.value() << " where id='"<< _id.value() 
			<< "' and attr='" << IS_DYNAMIC.value() << "') update " << _table.value() 
			<< " set date='" << SpCmnDate() << "', value='(null)'"
			<< " where id='" << _id.value() << "' and attr='" << IS_DYNAMIC.value() 
			<< "' else insert into " << _table.value() << " values ('" 
			<< SpCmnDate() << "', '" << _id.value() << "', '" << IS_DYNAMIC.value() << "', '(null)')";

		// Insertion de l'enregistrement caracterisant l'object comme etant dynamique.
		std::ostringstream req_sv_type;
		// On v�rifie l'existence de cet enregistrement pour ne pas violer la contrainte de clef primaire
		req_sv_type << "if exists(select * from " << _table.value() << " where id='"<< _id.value() 
			<< "' and attr='" << SV_TYPE.value() << "') update " << _table.value() 
			<< " set date='" << SpCmnDate() << "', value='" << getModel()->getSvTypeName() << "'"
			<< " where id='" << _id.value() << "' and attr='" << SV_TYPE.value() 
			<< "' else insert into " << _table.value() << " values ('" 
			<< SpCmnDate() << "', '" << _id.value() << "', '" << SV_TYPE.value() << "', '" << getModel()->getSvTypeName() << "')";

		// Ajout dans la file de requete.
		data_base->asyncLog( req_dyn.str() );
		data_base->asyncLog( req_sv_type.str() );
	}
	
	std::ostringstream req;

	// Traitement de l'attribut notifi�.
	// Si l'attribut notifie n'a pas ete cree.
	if ( !rep->isInDynList( _id, attr ) )
	{
		// Alors on l'ajoute a la liste des elements inseres.
		rep->addDynList( _id, attr );

		// Store de l'attribut d'objet avec v�rification d'existence dans la base.
		// En effet, au red�marrage, il peut y avoir un d�calage entre le contenu de la base
		// et la liste des attributs dynamiques.


		req << "if not exists(select * from " << _table.value() << " where id='"<< _id.value() 
			<< "' and attr='" << attr.value() << "') insert into " << _table.value() 
			<< " values ('" << SpCmnDate() << "', '" << _id.value() << "', '" << attr.value() 
			<< "', '" << SpStoreRp::getSQLCompliantString(value) << "') else ";
	}
	// mise a jour de l'attribut d'objet.
	req << "update " << _table.value() 
		<< " set value = '" << SpStoreRp::getSQLCompliantString(value)
		<< "', date = '" << SpCmnDate()
		<< "' where (id = '" << _id.value() << "' AND attr = '" << attr.value() << "')";
	
	// Ajout dans la file de requete (insert/update).
	data_base->asyncLog( req.str() );
}


//// ----------------------------------------------------------------------------------------------
//// Operation: setOwner : SpStoreDynRp
//
//// Semantics:
//   M�thode appel�e sur mise � jour de la relation inverse du Rp avec son conteneur.
//// ----------------------------------------------------------------------------------------------
void SpStoreDynRp::setOwner(SpStoreRp* owner)
{
	_owner = owner;

	SpStoreRepresentation* rep = dynamic_cast<SpStoreRepresentation*>(getRepresentation());

	if ( (rep != 0) && rep->isCreated() && (owner != 0) )
	{
		IlsString _table = rep->getTable();

		std::ostringstream req;

		// Insertion de l'enregistrement caracterisant l'object comme ayant un owner.
		// et on v�rifie l'existence de cet enregistrement pour ne pas violer la contrainte de clef primaire
		req << "if exists(select * from " << _table.value() << " where id='"<< _id.value() 
			<< "' and attr='HAS_OWNER') update " << _table.value() 
			<< " set date='" << SpCmnDate() << "', value='" << owner->getId().value() << "'"
			<< " where id='" << _id.value() << "' and attr='HAS_OWNER'" 
			<< " else insert into " << _table.value() << " values ('" 
			<< SpCmnDate() << "', '"
			<< _id.value() << "', 'HAS_OWNER', '" << owner->getId().value() << "')";

		// Ajout dans la file de requete.
		SpStoreDBMgr::GetInstance()->asyncLog( req.str() ); // TAB SpStoreDBManager -> SpStoreDBMgr
	}
}

//// ----------------------------------------------------------------------------------------------
ILS_REPRESENTATION_BEGIN(SpStoreRepresentation)
ILS_RP_ATTR_STRING(SpStoreRepresentation, sqlhostname, setHost)
ILS_RP_ATTR_STRING(SpStoreRepresentation, sqlschema, setSchema)
ILS_RP_ATTR_STRING(SpStoreRepresentation, sqltable, setTable)
ILS_REPRESENTATION_END(SpStoreRepresentation)


//// ----------------------------------------------------------------------------------------------
ILS_RP_OBJECT_BEGIN(SpStoreRp)
ILS_RP_ATTR_STRING(SpStoreRp,id,setId)
ILS_RP_DEFAULT_BOOLEAN(SpStoreRp,store) 
ILS_RP_DEFAULT_CHAR(SpStoreRp,store)
ILS_RP_DEFAULT_INT(SpStoreRp,store)
ILS_RP_DEFAULT_FLOAT(SpStoreRp,store) 
ILS_RP_DEFAULT_DOUBLE(SpStoreRp,store)
ILS_RP_DEFAULT_STRING(SpStoreRp,store) 
ILS_RP_DEFAULT_USERTYPE(SpStoreRp,SpCmnAccess,store) // to force link
ILS_RP_DEFAULT_USERTYPE(SpStoreRp,SpCmnDate,store)
ILS_RP_DEFAULT_USERTYPE(SpStoreRp,SpCmnIntervalTime,store)
ILS_RP_DEFAULT_USERTYPE(SpStoreRp,SpCmnMvValueMap,store)
ILS_RP_DEFAULT_USERTYPE(SpStoreRp,SpCmnMvValueVector,store)
ILS_RP_OBJECT_END(SpStoreRp)


//// ----------------------------------------------------------------------------------------------
ILS_RP_OBJECT_BEGIN(SpStoreEmptyRp)
ILS_RP_OBJECT_END(SpStoreEmptyRp)


//// ----------------------------------------------------------------------------------------------
ILS_RP_OBJECT1_BEGIN(SpStoreDynRp,SpStoreRp)
ILS_RP_ATTR_STRING(SpStoreDynRp,id,setId)
ILS_RP_ATTR_REF(SpStoreDynRp, OWNER, setOwner, SpStoreRp)
ILS_RP_ATTR_REF(SpStoreDynRp, OWNER, setOwner, SpStoreDynRp)
ILS_RP_DEFAULT_BOOLEAN(SpStoreDynRp,store) 
ILS_RP_DEFAULT_CHAR(SpStoreDynRp,store)
ILS_RP_DEFAULT_INT(SpStoreDynRp,store)
ILS_RP_DEFAULT_FLOAT(SpStoreDynRp,store) 
ILS_RP_DEFAULT_DOUBLE(SpStoreDynRp,store)
ILS_RP_DEFAULT_STRING(SpStoreDynRp,store) 
ILS_RP_DEFAULT_USERTYPE(SpStoreDynRp,SpCmnAccess,store) // to force link
ILS_RP_DEFAULT_USERTYPE(SpStoreDynRp,SpCmnDate,store)
ILS_RP_DEFAULT_USERTYPE(SpStoreDynRp,SpCmnIntervalTime,store)
ILS_RP_DEFAULT_USERTYPE(SpStoreDynRp,SpCmnMvValueMap,store)
ILS_RP_DEFAULT_USERTYPE(SpStoreDynRp,SpCmnMvValueVector,store)
ILS_RP_DEFAULT_FILE(SpStoreDynRp,store)
ILS_RP_OBJECT_END(SpStoreDynRp)
