#ifdef DB_ENGINE_ODBC
#include <SpStore/Interface/SpODBC.h>
#include <iostream>

using namespace std;

extern char* getCmdOption(char ** begin, char ** end, const std::string & option);

extern bool cmdOptionExists(char** begin, char** end, const std::string& option);

extern void doHelp();

int main_4(int argc, char **argv)
{
	std::string sDB("SQLEXPRESS");
	std::string sMethod("direct");
	std::string sUser("");
	std::string spasswrd("");
	std::string sRq("SELECT * FROM userinfotbl WHERE id = ?");
	//std::string sMethod("direct");

	if (cmdOptionExists(argv, argv + argc, "-h") || cmdOptionExists(argv, argv + argc, "-help"))
	{
		doHelp();
	}

	if (cmdOptionExists(argv, argv + argc, "-cs"))
	{
		sDB = getCmdOption(argv, argv + argc, "-cs");
	}

	if (cmdOptionExists(argv, argv + argc, "-m"))
	{
		sMethod = string(getCmdOption(argv, argv + argc, "-m"));
	}

	SpODBC::Connection my_Connection;
	SpODBC::Command my_Command;

	// Create a Connection with an ODBC Data Source
	if (!my_Connection.Connect(sDB, "", ""))
	{
		cout << "Cannot connect to the Data Source" << endl
			<< my_Connection.LastError();
		return 1;
	}

	// Prepare the query
	if (!my_Command.prepare(my_Connection, sRq))
	{
		cout << "Cannot prepare query!" << endl
			<< my_Command.LastError();
		return 2;
	}

	// Execute it with parameter (author_id) "3"
	my_Command.param(1).set_as_long(3);
	if (!my_Command.Execute())
	{
		cout << "Cannot execute prepared query!" << endl
			<< my_Command.LastError();
		return 2;
	}

	// Get results from Command
	while (my_Command.FetchNext())
	{
		// print all fields for each row
		for (int i = 1; i <= my_Command.count_columns(); i++)
			cout << my_Command.field(i).as_string() << "\t";

		cout << endl;
	}

	if (!my_Command.prepare(my_Connection, sRq))
	{
		cout << "Cannot prepare query!" << endl
			<< my_Command.LastError();
		return 2;
	}

	// Execute again with parameter (author_id) "4"
	my_Command.param(1).set_as_long(4);
	if (!my_Command.Execute())
	{
		cout << "Cannot execute prepared query!" << endl
			<< my_Command.LastError();
		return 2;
	}

	// Get results from Command
	while (my_Command.FetchNext())
	{
		// print all fields for each row
		for (int i = 1; i <= my_Command.count_columns(); i++)
			cout << my_Command.field(i).as_string() << "\t";

		cout << endl;
	}
	return 0;
}
#endif // DB_ENGINE_ODBC