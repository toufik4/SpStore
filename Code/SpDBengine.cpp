#include <SpStore/Interface/SpDBEngine.h>

//.=================================================================
SpDBEngine::t_Row::t_Row() :
_bound(false)
{
}

//.=================================================================
SpDBEngine::t_IdRow::t_IdRow(SpDBEngine* data_base) :
_data_base(data_base)
{
}

//.=================================================================
SpDBEngine::t_RestoreRow::t_RestoreRow(SpDBEngine* data_base) :
t_IdRow(data_base)
{
}

//.=================================================================
void SpDBEngine::t_IdRow::bind()
{
    if (!_bound && _data_base->isGoodConnection()) {
		bindRegularResultColumns();
		_bound = true;
	}
	
}

//.=================================================================
void SpDBEngine::t_IdRow::bindRegularResultColumns()
{
	//dbbind (dbproc,1,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ID_LENGTH,(unsigned char *)_id); 
	//_data_base->bind1(_id);
	_data_base->bind(*this);
}

//.=================================================================
void SpDBEngine::t_RestoreRow::bindRegularResultColumns()
{
	// t_IdRow::bindRegularResultColumns(dbproc);
	// dbbind (dbproc,2,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_ATTR_LENGTH,(unsigned char *)_attr);
	// dbbind (dbproc,3,NTBSTRINGBIND,(DBINT)STORE_MANAGER_MAX_VALUE_LENGTH,(unsigned char *)_value);

	// _data_base->bind2(_id,_attr,_value);
	_data_base->bind(*this);
}
